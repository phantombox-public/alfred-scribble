#!/usr/bin/php
<?php
/**
 * @author Sellim Dauti (Phantombox)
 * @link https://phantom-box.com
 */

if (empty($_ENV['outputFolder'])) {
    throw new RuntimeException('`outputFolder` is not defined in workflow settings');
}

$outputFolder = realpath($_ENV['outputFolder']);

if (!is_dir($outputFolder)) {
    throw new RuntimeException('Please enter absolute path as `outputFolder` and make sure that the directory exists');
}

if (empty($_ENV['convertPath'])) {
    throw new RuntimeException('`convertPath` is not defined in workflow settings');
}

$convertPath = $_ENV['convertPath'];

$queryParams = preg_split('/\s+/', $argv[1]);

$xSize = 1920;
$ySize = 1080;

$date = new DateTime();
$fileName = 'scribble-' . $date->format('Y-m-d-H-i-s');

if (isset($queryParams[0]) && is_numeric($queryParams[0])) {
    $xSize = $queryParams[0];
}

if (isset($queryParams[1]) && is_numeric($queryParams[1])) {
    $ySize = $queryParams[1];
}

$filePath = sprintf('%s/%s.png', $outputFolder, $fileName);
$command = sprintf('%s -size %dx%d xc:white %s', $convertPath, $xSize, $ySize, $filePath);
shell_exec($command);

echo $filePath;
