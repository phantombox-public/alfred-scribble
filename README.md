# Alfred Scribble

# Requirements
```
brew install imagemagick
```

# Install
Download or clone repository and you will find the workflow file in the dist/ folder

Please make sure to edit the workflow variables for your needs

# Usage
### Open recent screenshots in preview app
```
.srs
```

### Create new scribble file in preview app
```
.sn
.sn 500 500
```

Default size for new scribble images is 1920x1080 pixels
